package ro.ubb.catalog.web.converter2;

import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.BaseEntity;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.web.dto.ClientDto;

import java.util.stream.Collectors;

@Component
public class ClientConverter extends AbstractConverterBaseEntityConverter<Client, ClientDto> {
    @Override
    public Client convertDtoToModel(ClientDto dto) {
        Client client = Client.builder().name(dto.getName())
                .build();
        client.setId(dto.getId());
        return client;
    }

    @Override
    public ClientDto convertModelToDto(Client client) {
        ClientDto clientDto = ClientDto.builder().name(client.getName())
                .books(client.getBooks().stream().map(BaseEntity::getId).collect(Collectors.toSet()))
                .build();
        clientDto.setId(client.getId());
        return clientDto;
    }
}
