package ro.ubb.catalog.web.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.sql.Timestamp;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class PurchaseDto  {
    private Long idClient;
    private Long idBook;
    private String bookName;
    private String date;


}
