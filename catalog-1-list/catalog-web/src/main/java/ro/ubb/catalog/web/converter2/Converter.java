package ro.ubb.catalog.web.converter2;

/**
 * Created by radu.
 */
public interface Converter<Model, Dto> {
    Model convertDtoToModel(Dto dto);

    Dto convertModelToDto(Model model);
}
