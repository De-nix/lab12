package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.model.Exceptions.ValidatorException;
import ro.ubb.catalog.core.service.BookService;
import ro.ubb.catalog.web.converter2.BookConverter;
import ro.ubb.catalog.web.dto.BookDto;
import ro.ubb.catalog.web.dto.ClientDto;
import ro.ubb.catalog.web.dto.SortDto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class BookController {
    public static final Logger log= LoggerFactory.getLogger(ClientController.class);
    @Autowired
    BookConverter bookConverter;
    @Autowired
    BookService bookService;
    @RequestMapping(value = "/books", method = RequestMethod.GET)
    Set<BookDto> getBooks() {
        log.trace("getBooks - method entered");
        Set<BookDto> result = new HashSet<>(bookConverter
                .convertModelsToDtos(bookService.getAllBooks()));
        log.trace("getBooks - method finished");
         return result;

    }

//    to do: sort -> dto2
    @RequestMapping(value = "/books/sort",method = RequestMethod.GET)
    List<BookDto> getBooksSorted(@RequestBody Sort sort) {
        log.trace("getBooksSorted - method entered ");

        List<BookDto> result = new ArrayList<>(bookConverter
                .convertModelsToDtosSorted(bookService.getSortedBooks(sort)));
        log.trace("getBooksSorted - method finished");
        return result;

    }

    @RequestMapping(value = "/books/{id}", method = RequestMethod.GET)
    BookDto getBook(@PathVariable Long id) {
        log.trace("getBook - method entered: id{}",id);
        BookDto result = bookConverter.convertModelToDto(bookService.getBook(id));
        log.trace("getBook - method finished");
        return result;
    }

    @RequestMapping(value = "/books", method = RequestMethod.POST)
    BookDto saveBook(@RequestBody BookDto bookDto) throws ValidatorException {

        log.trace("saveBook - method entered : bookDTO {}",bookDto);
        BookDto result = bookConverter.convertModelToDto(bookService.addBook(
                bookDto.getTitle(),bookDto.getAuthor(),bookDto.getPrice()
        ));
        log.trace("saveBook - method finished");
        return result;
    }

    @RequestMapping(value = "/books/{id}", method = RequestMethod.PUT)
    BookDto updateBook(@PathVariable Long id, @RequestBody BookDto bookDto) throws ValidatorException {

        log.trace("updateBook - method entered: id {}, bookDto {}",id,bookDto);
        BookDto result=  bookConverter.convertModelToDto( bookService.updateBook(id,
                bookDto.getTitle(),bookDto.getAuthor(),bookDto.getPrice()));
        log.trace("updateBook - method finished");
        return result;
    }

    @RequestMapping(value = "/books/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteBook(@PathVariable Long id) {
        log.trace("deleteBook - method entered: id {}",id);
        bookService.deleteBook(id);
        log.trace("deleteBook - method finished");
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @RequestMapping(value = "/books/page/{page}", method = RequestMethod.GET)
    List<BookDto> getPageOfBooks(@PathVariable int page) {
        log.trace("getPageOfBooks - method entered: page{}",page);

        List<BookDto> result = bookService.getPageBooks(page).stream().map(x-> bookConverter.convertModelToDto(x)).collect(Collectors.toList());
        log.trace("getPageOfBooks  -  method finished");
        return result;
    }

    @RequestMapping(value = "/books/page/{page}", method = RequestMethod.POST)
    List<BookDto> sortPageOfBooks(@PathVariable int page, @RequestBody SortDto sort) {
        log.trace("getPageOfBooks - method entered: page{}",page);

        List<BookDto> result = bookService.getPageBooksSorted(page,sort.getSort()).stream().map(x-> bookConverter.convertModelToDto(x)).collect(Collectors.toList());
        log.trace("getPageOfBooks  - method finished");
        return result;
    }
    @RequestMapping(value = "/books/page/{page}/filter/title/{title}", method = RequestMethod.GET)
    List<BookDto> getPageFilteredByTitleOfBooks(@PathVariable int page,@PathVariable String title) {
        log.trace("getPageOfBooks - method entered: page{}",page);

        List<BookDto> result = bookService.getPageFilteredByTitleBooks(page,title).stream().
                map(x-> bookConverter.convertModelToDto(x)).collect(Collectors.toList());
        log.trace("getPageOfBooks  -  method finished");
        return result;
    }

    @RequestMapping(value = "/books/page/{page}/filter/title/{title}", method = RequestMethod.POST)
    List<BookDto> sortPageFilteredByTitleOfBooks(@PathVariable int page,@PathVariable String title, @RequestBody SortDto sort) {
        log.trace("getPageOfBooks - method entered: page{}",page);

        List<BookDto> result = bookService.getPageFilteredByTitleBooksSorted(page,title,sort.getSort()).stream().map(x-> bookConverter.convertModelToDto(x)).collect(Collectors.toList());
        log.trace("getPageOfBooks  - method finished");
        return result;
    }    @RequestMapping(value = "/books/page/{page}/filter/author/{author}", method = RequestMethod.GET)
    List<BookDto> getPageFilteredByAuthorOfBooks(@PathVariable int page,@PathVariable String author) {
        log.trace("getPageOfBooks - method entered: page{}",page);

        List<BookDto> result = bookService.getPageFilteredByAuthorBooks(page,author).stream().map(x-> bookConverter.convertModelToDto(x)).collect(Collectors.toList());
        log.trace("getPageOfBooks  -  method finished");
        return result;
    }

    @RequestMapping(value = "/books/page/{page}/filter/author/{author}", method = RequestMethod.POST)
    List<BookDto> sortPageFilteredByAuthorOfBooks(@PathVariable int page,@PathVariable String author, @RequestBody SortDto sort) {
        log.trace("getPageOfBooks - method entered: page{}",page);

        List<BookDto> result = bookService.getPageFilteredByAuthorBooksSorted(page,author,sort.getSort()).stream().map(x-> bookConverter.convertModelToDto(x)).collect(Collectors.toList());
        log.trace("getPageOfBooks  - method finished");
        return result;
    }    @RequestMapping(value = "/books/page/{page}/filter/price/{price}", method = RequestMethod.GET)
    List<BookDto> getPageFilteredByPriceOfBooks(@PathVariable int page,@PathVariable int price) {
        log.trace("getPageOfBooks - method entered: page{}",page);

        List<BookDto> result = bookService.getPageFilteredByPriceBooks(page,price).stream().map(x-> bookConverter.convertModelToDto(x)).collect(Collectors.toList());
        log.trace("getPageOfBooks  -  method finished");
        return result;
    }

    @RequestMapping(value = "/books/page/{page}/filter/price/{price}", method = RequestMethod.POST)
    List<BookDto> sortPageFilteredByPriceOfBooks(@PathVariable int page,@PathVariable int price, @RequestBody SortDto sort) {
        log.trace("getPageOfBooks - method entered: page{}",page);

        List<BookDto> result = bookService.getPageFilteredByPriceBooksSorted(page,price,sort.getSort()).stream().map(x-> bookConverter.convertModelToDto(x)).collect(Collectors.toList());
        log.trace("getPageOfBooks  - method finished");
        return result;
    }
    
}
