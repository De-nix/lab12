package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.model.Exceptions.ValidatorException;
import ro.ubb.catalog.core.service.ClientService;
import ro.ubb.catalog.web.converter2.ClientConverter;
import ro.ubb.catalog.web.dto.ClientDto;
import ro.ubb.catalog.web.dto.SortDto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class ClientController {

    public static final Logger log= LoggerFactory.getLogger(ClientController.class);
    @Autowired
    ClientConverter clientConverter;
    @Autowired
    ClientService clientService;
    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    Set<ClientDto> getClients() {
        log.trace("getClients - method entered");
        Set<ClientDto> result = new HashSet<>(clientConverter.convertModelsToDtos(clientService.getAllClients()));
        log.trace("getClients - method finished");
        return result;
    }


    @RequestMapping(value = "/clients/{id}", method = RequestMethod.GET)
    ClientDto getClient(@PathVariable Long id) {
        log.trace("getClient - method entered: id{}",id);
        ClientDto result = clientConverter.convertModelToDto(clientService.getClient(id));
        log.trace("getClient - method finished");
        return result;
    }

    @RequestMapping(value = "/clients/page/{page}", method = RequestMethod.GET)
    List<ClientDto> getPageOfClients(@PathVariable int page) {
        log.trace("getPageOfClients - method entered: id{}",page);
        List<ClientDto> result = clientService.getPageClients(page).stream().map(x-> clientConverter.convertModelToDto(x)).collect(Collectors.toList());
        log.trace("getPageOfClients - method finished");
        return result;
    }

    @RequestMapping(value = "/clients/page/{page}", method = RequestMethod.POST)
    List<ClientDto> sortPageOfClients(@PathVariable int page, @RequestBody SortDto sort) {
        log.trace("getPageOfClients - method entered: id{} sort{}",page, sort);
        List<ClientDto> result = clientService.getPageClientsSorted(page,sort.getSort()).stream().map(x-> clientConverter.convertModelToDto(x)).collect(Collectors.toList());
        log.trace("getPageOfClients - method finished");
        return result;
    }

    @RequestMapping(value = "/clients/page/{page}/filter/name/{name}", method = RequestMethod.GET)
    List<ClientDto> getFilteredPageOfClients(@PathVariable int page, @PathVariable String name) {
        log.trace("getPageOfClients - method entered: id{}",page);
        List<ClientDto> result = clientService.getPageFilteredByNameClients(page, name).stream().
                map(x-> clientConverter.convertModelToDto(x)).collect(Collectors.toList());
        log.trace("getPageOfClients - method finished");
        return result;
    }

    @RequestMapping(value = "/clients/page/{page}/filter/name/{name}", method = RequestMethod.POST)
    List<ClientDto> sortFilteredPageOfClients(@PathVariable int page,@PathVariable String name, @RequestBody SortDto sort) {
        log.trace("getPageOfClients - method entered: id{} sort{}",page, sort);


        List<ClientDto> result = clientService.getPageFilteredByNameClientsSorted(page,name,sort.getSort()).stream().
                map(x-> clientConverter.convertModelToDto(x)).collect(Collectors.toList());
        log.trace("getPageOfClients - method finished");
        return result;
    }

    @RequestMapping(value = "/clients", method = RequestMethod.POST)
    ClientDto saveClient(@RequestBody ClientDto clientDto) throws ValidatorException {

        log.trace("saveClient - method entered : clientDTO {}",clientDto);
        ClientDto result = clientConverter.convertModelToDto(clientService.addClient(
                clientDto.getName()
        ));
        log.trace("saveClient - method finished");
        return result;
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.PUT)
    ClientDto updateClient(@PathVariable Long id, @RequestBody ClientDto clientDto) {

        log.trace("updateClient - method entered: id {}, clientDto {}",id,clientDto);

        ClientDto result = null;
        try {
            result = clientConverter.convertModelToDto( clientService.updateClient(id,
                    clientDto.getName(),clientDto.getBooks()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.trace("updateClient - method finished");
        return result;
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteClient(@PathVariable Long id) {
        log.trace("deleteClient - method entered: id {}",id);
        clientService.deleteClient(id);
        log.trace("deleteClient - method finished");
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
