package ro.ubb.catalog.core.service;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import ro.ubb.catalog.core.model.BaseEntity;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Exceptions.ValidatorException;

import java.util.List;
import java.util.Set;

public interface BookService {


    Book addBook(String title, String author, int price) throws ValidatorException;

    /**
     * @param id - the id of the book that will be deleted
     * @return
     */
     void deleteBook(Long id) ;


     Book updateBook(Long id, String title, String author, int price) throws ValidatorException ;

    /**
     * @param id - id of the book
     * @return - the book with id = 'id'
     */
    Book getBook(Long id);

    Set<Book> getAllBooks();


    List<Book> getSortedBooks(Sort sort);
    List<Book> getPageBooks(int page);
    List<Book> getPageBooksSorted(int page, Sort sort);
    public List<Book> getPageFilteredByTitleBooks(int page, String title);
    public List<Book> getPageFilteredByTitleBooksSorted(int page,String title, Sort sort);
    public List<Book> getPageFilteredByAuthorBooks(int page, String author);
    public List<Book> getPageFilteredByAuthorBooksSorted(int page, String author,Sort sort);
    public List<Book> getPageFilteredByPriceBooks(int page, int price);
    public List<Book> getPageFilteredByPriceBooksSorted(int page, int price, Sort sort);
}
