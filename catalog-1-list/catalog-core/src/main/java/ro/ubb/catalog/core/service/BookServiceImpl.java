package ro.ubb.catalog.core.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Exceptions.ValidatorException;
import ro.ubb.catalog.core.model.Validators.BookValidator;
import ro.ubb.catalog.core.repository.BookRepository;
import ro.ubb.catalog.core.repository.ClientRepository;

import javax.persistence.EntityManager;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class BookServiceImpl implements BookService{
    static int pageCapacity =5;

    @Autowired
    private BookRepository bookRepository;



    public static final Logger log = LoggerFactory.getLogger(BookServiceImpl.class);
    public  Book addBook(String title,String author, int price) throws ValidatorException {
        Book newBook = Book.builder().title(title).author(author).price(price).purchases(new HashSet<>()).build();
        BookValidator.validate(newBook);
        return bookRepository.save(newBook);
    }

    public  void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }
    @Transactional
    public  Book updateBook(Long id, String title,String author, int price) throws ValidatorException {
        log.trace("updateBook - method entered:id={} title={} author={} price={}", id,title,author,price);
        Optional<Book> optionalBook =  bookRepository.findById(id);
        optionalBook.ifPresent(s -> {
            s.setTitle(title);
            s.setAuthor(author);
            s.setPrice(price);
            log.debug("updateBook - updated: s={}", s);
        });
        log.trace("updateBook - method finished");
        return optionalBook.orElse(null);


    }
    public  Book getBook(Long id) {
        return bookRepository.findById(id).orElse(null);
    }


    public  Set<Book> getAllBooks() {
        Iterable<Book> Books = bookRepository.findAll();
        return StreamSupport.stream(Books.spliterator(), false).collect(Collectors.toSet());
    }

    @Override
    public List<Book> getSortedBooks(Sort sort) {
        return bookRepository.findAll(sort);
    }

    public List<Book> getPageBooks(int page){
        return bookRepository.findAll(PageRequest.of(page,pageCapacity)).getContent();
    }
    public List<Book> getPageBooksSorted(int page, Sort sort){
        log.trace("sort from getbooks sorted {}",sort);
        return bookRepository.findAll(PageRequest.of(page,pageCapacity, sort)).getContent();
    }
    public List<Book> getPageFilteredByTitleBooks(int page, String title){
        return bookRepository.findAllByTitleContaining(title, PageRequest.of(page,pageCapacity)).getContent();
    }
    public List<Book> getPageFilteredByTitleBooksSorted(int page,String title, Sort sort){
        return bookRepository.findAllByTitleContaining(title,PageRequest.of(page,pageCapacity, sort)).getContent();
    }
    public List<Book> getPageFilteredByAuthorBooks(int page, String author){
        return bookRepository.findAllByAuthorContaining(author,PageRequest.of(page,pageCapacity)).getContent();
    }
    public List<Book> getPageFilteredByAuthorBooksSorted(int page, String author,Sort sort){
        return bookRepository.findAllByAuthorContaining(author,PageRequest.of(page,pageCapacity, sort)).getContent();
    }
    public List<Book> getPageFilteredByPriceBooks(int page, int price){
        return bookRepository.findAllByPriceLessThan(price,PageRequest.of(page,pageCapacity)).getContent();
    }
    public List<Book> getPageFilteredByPriceBooksSorted(int page, int price, Sort sort){
        return bookRepository.findAllByPriceLessThan(price,PageRequest.of(page,pageCapacity, sort)).getContent();
    }

}
