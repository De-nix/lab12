package ro.ubb.catalog.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Client;

@Repository
public interface ClientRepository extends StoreRepository<Client, Long> {

    Page<Client> findAllByNameContaining(String name, Pageable pageable);
}
