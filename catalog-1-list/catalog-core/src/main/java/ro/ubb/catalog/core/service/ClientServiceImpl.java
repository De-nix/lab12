package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.orm.hibernate5.HibernateOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.model.Exceptions.RepositoryException;
import ro.ubb.catalog.core.model.Exceptions.ValidatorException;
import ro.ubb.catalog.core.model.Purchase;
import ro.ubb.catalog.core.model.Validators.ClientValidator;
import ro.ubb.catalog.core.repository.BookRepository;
import ro.ubb.catalog.core.repository.ClientRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Service
public class ClientServiceImpl implements ClientService {

    static int pageCapacity =5;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private BookRepository bookRepository;

    public static final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);

    public  Client addClient(String name) throws ValidatorException {
        Client newClient = Client.builder().name(name).build();
        ClientValidator.validate(newClient);
        return clientRepository.save(newClient);
    }

    public  void deleteClient(Long id) {
        clientRepository.deleteById(id);
    }

    public  List<Client> getClientsSortedByName(Sort sort) {
        return clientRepository.findAll(sort);
    }



    public  Client getClient(Long id) {
        return clientRepository.findById(id).orElse(null);
    }

    @Transactional
    public Client updateClient(Long id, String name, Set<Long> books) throws Exception {

        log.trace("updateClient - method entered: id ={}, client={} books{}", id, name,books);
        clientRepository.findById(id).ifPresent(s -> {
            s.setName(name);
            log.trace("initial nr of bought books {}",s.getPurchases().size());
            s.getPurchases().stream().filter(x -> !books.contains(x.getBook().getId())).collect(Collectors.toList()).forEach(
            x->{
                log.trace("in delete - the deleted entity is {}, with fields {},{},{}",x,x.getBook(),x.getClient(),x.getDate());
                s.deletePurchase(x.getBook().getId());
                x.getBook().deletePurchase(s.getId());

                log.trace("in delete - the deleted entity after the first attempt of deleting entity from client is {}, with fields {},{},{}",x,x.getBook(),x.getClient(),x.getDate());

                    log.trace("after delete -- nr of bought books {}",s.getPurchases().size());
            });
            log.trace("exiting the delete part of update -> moving to the insert one which works");

            Set<Long> ownedBooks = s.getPurchases().stream().map(y -> y.getBook().getId()).collect(Collectors.toSet());
            books.stream().filter(x -> !ownedBooks.contains(x)).forEach(x -> {
                Book book = bookRepository.findById(x)
                        .orElseThrow(() -> new RepositoryException("The id's of the books are not valid ones"));
                s.addPurchase(book, new Timestamp(System.currentTimeMillis()));
            });
            log.debug("updateClient - updated: s={}", s);

        });


        log.trace("updateClient - method finished");
        return clientRepository.findById(id).orElseThrow(()->new Exception("the client was not found"));
    }

    public  Set<Client> getAllClients() {
        Iterable<Client> clients = clientRepository.findAll();
        return StreamSupport.stream(clients.spliterator(), false).collect(Collectors.toSet());
    }

    @Override
    public List<Client> getSortedClients(Sort sort) {
        return new ArrayList<>(clientRepository.findAll(sort));
    }

    @Transactional
    public void updatePurchaseDate(Long clientId, Long bookId, Timestamp date){
        log.trace("intrare update dates din service Server client{}, book{}, date{}",clientId,bookId,date);

        Optional<Client> result = clientRepository.findById(clientId);
        Purchase p = result.get().getPurchases().stream().filter(y-> y.getBook().getId().equals(bookId)).findFirst().get();
        p.setDate(date);

        log.trace("client after purchase update is {}",result.get());


        log.trace("client after purchase update is {}",result.get());

        log.trace("iesire update dates din service Server");
    }
    public List<Client> getPageClients(int page){
        return clientRepository.findAll(PageRequest.of(page,pageCapacity)).getContent();
    }
    public List<Client> getPageClientsSorted(int page, Sort sort){
        return clientRepository.findAll(PageRequest.of(page,pageCapacity, sort)).getContent();
    }
    public List<Client> getPageFilteredByNameClients(int page, String name){
        return clientRepository.findAllByNameContaining(name,PageRequest.of(page,pageCapacity)).getContent();
    }
    public List<Client> getPageFilteredByNameClientsSorted(int page, String name,Sort sort){
        return clientRepository.findAllByNameContaining(name,PageRequest.of(page,pageCapacity, sort)).getContent();
    }

}
