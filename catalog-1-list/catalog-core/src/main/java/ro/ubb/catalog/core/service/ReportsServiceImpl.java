package ro.ubb.catalog.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Client;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ReportsServiceImpl implements ReportsService {
    @Autowired
    BookService bookServiceImpl;
    @Autowired
    ClientService clientServiceImpl;

    /**
     * @param no - the given minimum number of books that a client should have
     * @return - the set of clients that have more than x books
     */
    public  Set<Client> filterClientsWithMoreThanXBooks(int no) {
        return clientServiceImpl.getAllClients().stream().
                filter(x ->
                        no < x.getPurchases().size()).
                collect(Collectors.toSet());
    }


    public  Integer spentMoney(Client client) {
        return client.getBooks().stream().map(Book::getPrice).reduce(0,Integer::sum);
    }

    /**
     * @return sorted list of the clients by the total amount of spent money
     */
    public  List<Client> sortClientsByMoneySpent() {

        List<Client> clients = new ArrayList<>(clientServiceImpl.getAllClients());
        clients.sort(Comparator.comparing(this::spentMoney));
        return clients;
    }


    /**
     * Returns all Books whose name contain the given string.
     *
     * @param s - the given string
     * @return - set of books that contains the given string
     */
    public  Set<Book> filterBooksByTitles(String s) {
        Set<Book> filteredBooks = bookServiceImpl.getAllBooks();
        filteredBooks.removeIf(Book -> !Book.getTitle().contains(s));
        return filteredBooks;
    }

    /**
     * @param s - the given price
     * @return - a set of books that have the price greater than s
     */

    public  Set<Book> removeBookIfPriceLessThenS(int s) {
        Set<Book> filteredBooks = bookServiceImpl.getAllBooks();
        filteredBooks.removeIf(book -> book.getPrice() <= s);
        return filteredBooks;
    }

    /**
     * @param s - a given string
     * @return - the set of clients whose names contain that string
     */

    public  Set<Client> filterClientsByName(String s) {

        Set<Client> filteredClients = clientServiceImpl.getAllClients();
        filteredClients.removeIf(client -> !client.getName().contains(s));
        return filteredClients;
    }


}
