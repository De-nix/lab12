package ro.ubb.catalog.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import ro.ubb.catalog.core.model.Book;

import java.util.List;

/**
 * Created by radu.
 */

@Repository
public interface BookRepository extends StoreRepository<Book, Long> {
    Page<Book> findAllByTitleContaining(String title, Pageable pageable);
    Page<Book> findAllByAuthorContaining(String author, Pageable pageable);
    Page<Book> findAllByPriceLessThan(Integer price, Pageable pageable);
}
