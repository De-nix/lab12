package ro.ubb.catalog.core.model;

import lombok.*;

import java.io.Serializable;

/**
 * Created by radu.
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
class PurchasePK implements Serializable {
    private Client client;
    private Book book;
}
