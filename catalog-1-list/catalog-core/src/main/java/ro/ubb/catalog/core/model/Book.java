package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by radu.
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Book extends BaseEntity<Long> {

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "author", nullable = false)
    private String author;

    @Column(name = "price", nullable = false)
    private Integer price;


    @OneToMany(mappedBy = "book", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Purchase> purchases;

    public Set<Client> getClients() {
        return purchases.stream()
                .map(Purchase::getClient).collect(Collectors.toUnmodifiableSet());
    }

    public void addClient(Client client) {
        Purchase purchase = new Purchase();
        purchase.setClient(client);
        purchase.setBook(this);
        purchases.add(purchase);
    }

    public void addPurchase(Client client, Timestamp date) {
        Purchase purchase = new Purchase();
        purchase.setClient(client);
        purchase.setDate(date);
        purchase.setBook(this);
        purchases.add(purchase);
    }
    public void deletePurchase(Long clientId){
        purchases.removeIf(x-> x.getClient().getId().equals(clientId));

    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book that = (Book) o;

        return title.equals(that.title);
    }

    @Override
    public int hashCode() {
        return title.hashCode();
    }


    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", price=" + price +
                "} " + super.toString();
    }
}
