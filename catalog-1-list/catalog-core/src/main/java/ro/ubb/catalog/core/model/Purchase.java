package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by radu.
 */

@Entity
@IdClass(PurchasePK.class)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Table(name = "purchase")
public class Purchase implements Serializable {

    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "client_id")
    private Client client;

    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "book_id")
    private Book book;

    @Column(name = "date")
    private Timestamp date;

    @Override
    public String toString() {
        return "Purchase{" +
                "client=" + client.getId() +
                ", book=" + book.getId() +
                ", date=" + date +
                '}';
    }
}
