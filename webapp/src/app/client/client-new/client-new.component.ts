import {Component, OnInit} from '@angular/core';
import {ClientService} from "../shared/client.service";
import {Location} from "@angular/common";

@Component({
  selector: 'app-client-add',
  templateUrl: './client-new.component.html',
  styleUrls: ['./client-new.component.css']
})
export class ClientNewComponent implements OnInit {

  constructor(private clientService: ClientService,
              private location: Location
  ) {
  }

  ngOnInit(): void {
  }

  saveClient( name: string) {
    console.log("saving client",  name);

    this.clientService.saveClient(
      name
    )
      .subscribe(client => console.log("saved client: ", client));

    this.location.back(); // ...
  }

  goBack(): void {
    this.location.back();
  }
}
