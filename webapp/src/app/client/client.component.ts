import {Component} from "@angular/core";
import {ClientService} from "./shared/client.service";
import {Router} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css'],
})
export class ClientComponent {
  constructor(private router: Router) {
  }

  addNewStudent() {
    console.log("add new student btn clicked ");

    this.router.navigate(["student/new"]);
  }
}
