import {Injectable} from '@angular/core';

import {HttpClient} from "@angular/common/http";

import {Client} from "./client.model";

import {Observable} from "rxjs";
import {map} from "rxjs/operators";


@Injectable()
export class ClientService {
  private clientsUrl = 'http://localhost:8080/api/clients';

  constructor(private httpClient: HttpClient) {
  }

  getClients(): Observable<Client[]> {
    return this.httpClient
      .get<Array<Client>>(this.clientsUrl);
  }

  getClient(id: number): Observable<Client> {
    return this.getClients()
      .pipe(
        map(clients => clients.find(client => client.id === id))
      );
  }
  getClientsByName(page: number, name: string): Observable<Client[]> {
    const url = this.clientsUrl + "/page/" + page + "/filter/name/" + name;
    return this.httpClient.get<Client[]>(url);
  }
  getClientsByNameSorted(page: number, name: string, sort: {"fields": string[], "directions": boolean[]}): Observable<Client[]> {
    const url = this.clientsUrl + "/page/" + page + "/filter/name/" + name;
    return this.httpClient.post<Client[]>(url, sort);
  }
  saveClient(name: string): Observable<Client> {
    const client = {name};
    console.log("saveClient", client);

    return this.httpClient
      .post<Client>(this.clientsUrl, client);
  }

  update(client): Observable<Client> {
    const url = `${this.clientsUrl}/${client.id}`;
    return this.httpClient
      .put<Client>(url, client);
  }

  deleteClient(id: number): Observable<Client> {
    const url = `${this.clientsUrl}/${id}`;
    return this.httpClient
      .delete<Client>(url);
  }

  getPageOfClients(value: number): Observable<Array<Client>> {
    const url = this.clientsUrl + "/page/" + value;
    return this.httpClient.get<Array<Client>>(url);
  }

  getSortedPageOfClients(valueAsNumber: number, value: {"fields": string[], "directions": boolean[]}): Observable<Array<Client>> {
    const url = this.clientsUrl + "/page/" + valueAsNumber;
    return this.httpClient.post<Array<Client>>(url, value);
  }
}
