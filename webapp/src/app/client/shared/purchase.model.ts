
export class Purchase {
  idClient: number;
  idBook: number;
  bookName: string;
  date: string;
}
