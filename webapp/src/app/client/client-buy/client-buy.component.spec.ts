import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientBuyComponent } from './client-buy.component';

describe('ClientBuyComponent', () => {
  let component: ClientBuyComponent;
  let fixture: ComponentFixture<ClientBuyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientBuyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientBuyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
