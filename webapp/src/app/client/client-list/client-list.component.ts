import {Component, OnInit} from '@angular/core';
import {Client} from "../shared/client.model";
import {ClientService} from "../shared/client.service";
import {Router} from "@angular/router";
import {Location} from "@angular/common";

@Component({
  moduleId: module.id,
  selector: 'app-client',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css'],
})
export class ClientListComponent implements OnInit {
  errorMessage: string;
  clients: Client[];
  selectedClient: Client;
  noClients: number;

  constructor(private clientService: ClientService,
              private router: Router, private location: Location) {
  }

  ngOnInit(): void {
    this.getClients();
  }

  getClients() {
    this.clientService.getClients()
      .subscribe(
        clients => {
          this.noClients = clients.length / 5;
          this.getPagedClients(0);
        }
      );
  }

  onSelect(client: Client): void {
    this.selectedClient = client;
  }

  getFilteredClients(page: number, name: string, value1: string, value2: string, value3: string, value4: string) {

    const sort = this.turnValuesIntoSort(value1, value2, value3, value4);
    if (sort.directions.length === 0) {
      this.clientService.getClientsByName(page, name)
        .subscribe(
          clients => this.clients = clients,
          error => this.errorMessage = <any>error
        );
    } else {
      this.clientService.getClientsByNameSorted(page, name, sort)
        .subscribe(
          clients => this.clients = clients,
          error => this.errorMessage = <any>error
        );
    }
  }


  gotoDetail(): void {
    this.router.navigate(['/client/detail', this.selectedClient.id]);
  }

  deleteClient(client: Client) {
    console.log("deleting client: ", client);

    this.clientService.deleteClient(client.id)
      .subscribe(_ => {
        console.log("client deleted");

        this.clients = this.clients.filter(s => s !== client);
        if (this.selectedClient === client) {
          this.selectedClient = null;
        }
      });
  }


  goBack(): void {
    this.location.back();
  }

  getPagedClients(value: number) {
    this.clientService.getPageOfClients(value).subscribe(
      clients => this.clients = clients
    );
  }

  turnValuesIntoSort(attr1: string, dir1: string, attr2: string, dir2: string): { "fields": string[], "directions": boolean[] } {
    const fileds = [];
    const dirs = [];
    if (attr1 !== "" && dir1 !== "") {
      fileds.push(attr1);
      dirs.push(dir1 === "true");
    }
    if (attr2 !== "" && dir2 !== "") {
      fileds.push(attr2);
      dirs.push(dir2 === "true");
    }
    return {"fields": fileds, "directions": dirs};
  }

  getSortedPagedClients(valueAsNumber: number, attr1: string, dir1: string, attr2: string, dir2: string) {

    this.clientService.getSortedPageOfClients(valueAsNumber, this.turnValuesIntoSort(attr1, dir1, attr2, dir2)).subscribe(
      clients => this.clients = clients
    );
  }
}
