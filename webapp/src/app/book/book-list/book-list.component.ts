import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {BookService} from "../shared/book.service";
import {Book} from "../shared/book.model";

import {Location} from '@angular/common';


@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {
  errorMessage: string;
  books: Book[];
  selectedBook: Book;
  noBooks: number;
  constructor(private bookService: BookService,
              private router: Router, private location: Location) { }

  ngOnInit(): void {
    this.bookService.getBooks().subscribe(
      books => {
        this.noBooks = books.length / 5;
        this.getPagedBooks(0);
      }
    );
  }


  getBooks() {
    this.bookService.getBooks()
      .subscribe(
        books => this.books = books,
        error => this.errorMessage = <any>error
      );
  }
  getBooksByTitle(title: string, page: number,  value1: string, value2: string, value3: string, value4: string,
                  value5: string, value6: string, value7: string, value8: string) {

      const sort = this.turnValuesIntoSort(value1, value2, value3, value4, value5, value6, value7, value8);
      if (sort.directions.length === 0) {
        this.bookService.getBooksFilteredByTitle(page, title)
          .subscribe(
            books => this.books = books,
            error => this.errorMessage = <any>error
          );
      } else {
        this.bookService.getBooksFilteredByTitleSorted(page, title, sort)
          .subscribe(
            books => this.books = books,
            error => this.errorMessage = <any>error
          );
      }
    }
    getBooksByAuthor(author: string, page: number, value1: string, value2: string, value3: string, value4: string,
                  value5: string, value6: string, value7: string, value8: string) {

      const sort = this.turnValuesIntoSort(value1, value2, value3, value4, value5, value6, value7, value8);
      if (sort.directions.length === 0) {
        this.bookService.getBooksFilteredByAuthor(page, author)
          .subscribe(
            books => this.books = books,
            error => this.errorMessage = <any>error
          );
      } else {
        this.bookService.getBooksFilteredByAuthorSorted(page, author, sort)
          .subscribe(
            books => this.books = books,
            error => this.errorMessage = <any>error
          );
      }
    }
    getBooksByPrice(price: number, page: number, value1: string, value2: string, value3: string, value4: string,
                  value5: string, value6: string, value7: string, value8: string) {

      const sort = this.turnValuesIntoSort(value1, value2, value3, value4, value5, value6, value7, value8);
      if (sort.directions.length === 0) {
        this.bookService.getBooksFilteredByPrice(page, price)
          .subscribe(
            books => this.books = books,
            error => this.errorMessage = <any>error
          );
      } else {
        this.bookService.getBooksFilteredByPriceSorted(page, price, sort)
          .subscribe(
            books => this.books = books,
            error => this.errorMessage = <any>error
          );
      }
    }
  turnValuesIntoSort(attr1: string, dir1: string, attr2: string, dir2: string, attr3: string, dir3: string, attr4: string, dir4: string
  ): { "fields": string[], "directions": boolean[] } {
  const fileds = [];
  const dirs = [];
  if (attr1 !== "" && dir1 !== "") {
    fileds.push(attr1);
    dirs.push(dir1 === "true");
  }
  if (attr2 !== "" && dir2 !== "") {
    fileds.push(attr2);
    dirs.push(dir2 === "true");
  }
  if (attr3 !== "" && dir3 !== "") {
    fileds.push(attr3);
    dirs.push(dir3 === "true");
  }
  if (attr4 !== "" && dir4 !== "") {
    fileds.push(attr4);
    dirs.push(dir4 === "true");
  }
  return {"fields": fileds, "directions": dirs};
}

  onSelect(book: Book): void {
    this.selectedBook = book;
  }

  gotoDetail(): void {
    this.router.navigate(['/book/detail', this.selectedBook.id]);
  }

  deleteBook(book: Book) {
    console.log("deleting book: ", book);

    this.bookService.deleteBook(book.id)
      .subscribe(_ => {
        console.log("book deleted");

        this.books = this.books
          .filter(s => s.id !== book.id);
      });
  }


  goBack(): void {
    this.location.back();
  }
  getPagedBooks(value: number): void {
    this.bookService.getPagedBooks(value).subscribe( books => this.books = books);
  }

  getSortedPagedBooks(valueAsNumber: number, value1: string, value2: string, value3: string, value4: string,
                      value5: string, value6: string, value7: string, value8: string) {
    this.bookService.getPagedBooksSorted(valueAsNumber, this.turnValuesIntoSort(value1, value2, value3, value4, value5, value6,
      value7, value8)).subscribe(
      books => this.books = books);
  }
}
