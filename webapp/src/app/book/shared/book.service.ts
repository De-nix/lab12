import {Injectable} from '@angular/core';

import {HttpClient} from "@angular/common/http";

import {Book} from "./book.model";

import {Observable} from "rxjs";
import {filter, map} from "rxjs/operators";


@Injectable()
export class BookService {
  private booksUrl = 'http://localhost:8080/api/books';

  constructor(private httpClient: HttpClient) {
  }

  getBooks(): Observable<Book[]> {
    return this.httpClient
      .get<Array<Book>>(this.booksUrl);
  }
  getBooksFilteredByTitle(page: number, title: string): Observable<Book[]> {
    const url = this.booksUrl + "/page/" + page + "/filter/title/" + title;
    return this.httpClient
      .get<Array<Book>>(url);
  }

  getBooksFilteredByPrice(page: number, price: number): Observable<Book[]> {

    const url = this.booksUrl + "/page/" + page + "/filter/price/" + price;
    return this.httpClient
      .get<Array<Book>>(url);
  }
  getBooksFilteredByAuthor(page: number, author: string): Observable<Book[]> {
    const url = this.booksUrl + "/page/" + page + "/filter/author/" + author;
    return this.httpClient
      .get<Array<Book>>(url);
  }
  getBooksFilteredByTitleSorted(page: number, title: string, sort: {"fields": string[], "directions": boolean[]}): Observable<Book[]> {
    const url = this.booksUrl + "/page/" + page + "/filter/title/" + title;
    return this.httpClient
      .post<Array<Book>>(url, sort);
  }

  getBooksFilteredByPriceSorted(page: number, price: number, sort: {"fields": string[], "directions": boolean[]}): Observable<Book[]> {

    const url = this.booksUrl + "/page/" + page + "/filter/price/" + price;
    return this.httpClient
      .post<Array<Book>>(url, sort);
  }
  getBooksFilteredByAuthorSorted(page: number, author: string, sort: {"fields": string[], "directions": boolean[]}): Observable<Book[]> {
    const url = this.booksUrl + "/page/" + page + "/filter/author/" + author;
    return this.httpClient
      .post<Array<Book>>(url, sort);
  }




  getBook(id: number): Observable<Book> {
    return this.getBooks()
      .pipe(
        map(books => books.find(book => book.id === id))
      );
  }

  saveBook(book: Book): Observable<Book> {
    console.log("saveBook", book);

    return this.httpClient
      .post<Book>(this.booksUrl, book);
  }

  update(book): Observable<Book> {
    const url = `${this.booksUrl}/${book.id}`;
    return this.httpClient
      .put<Book>(url, book);
  }

  deleteBook(id: number): Observable<any> {
    const url = `${this.booksUrl}/${id}`;
    return this.httpClient
      .delete(url);
  }

  getPagedBooks(value: number): Observable<Array<Book>> {
    const url = this.booksUrl + "/page/" + value;
    return this.httpClient.get<Array<Book>>(url);
  }

  getPagedBooksSorted(valueAsNumber: number, param2: { directions: any[]; fields: any[] }): Observable<Array<Book>> {
    const url = this.booksUrl + "/page/" + valueAsNumber;
    return this.httpClient.post<Array<Book>>(url, param2);
  }
}
